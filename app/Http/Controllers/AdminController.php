<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use App\Student;

class AdminController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
		$count=Student::select('user','bookingid','county','center','class','gender')->where('center','=','KEMI')->count();
		
		$count2=Student::select('user','bookingid','county','center','class','gender')->where('center','=','KU, Main Campus')->where('class','=','Week 1 - Classroom 2')->count();
		
		$count3=Student::select('user','bookingid','county','center','class','gender')->where('center','=','KU, Main Campus')->where('class','=','Week 1 - Classroom 3')->count();
		
		$count4=Student::select('user','bookingid','county','center','class','gender')->where('center','=','KU, Main Campus')->where('class','=','Week 1 - Classroom 4')->count();
		$count5=Student::select('user','bookingid','county','center','class','gender')->where('center','=','KU, Main Campus')->where('class','=','Week 1 - Classroom 5')->count();
		
		$count6=Student::select('user','bookingid','county','center','class','gender')->where('center','=','The Cooperative University of Kenya Meru')->count();
		
		$count7=Student::select('user','bookingid','county','center','class','gender')->where('center','=','KU, Mombasa City Campus')->count();
		
		
		$count8=Student::select('user','bookingid','county','center','class','gender')->where('center','=','RVIST, Main Campus')->count();
		
		$count9=Student::select('user','bookingid','county','center','class','gender')->where('center','=','Tom Mboya Labour College')->count();
          return view('admin.index')->with('count',$count)->with('count2',$count2)->with('count3',$count3)->with('count4',$count4)
			  ->with('count5',$count5)->with('count6',$count6)->with('count7',$count7)->with('count8',$count8)->with('count9',$count9);
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
	
	 public function hr()
    {
          return view('admin.hr');
    }
}
