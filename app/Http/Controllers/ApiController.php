<?php

namespace App\Http\Controllers;

use Request;
use Response;
use App\Http\Requests;

use Auth;

use DB;
use Redirect;
use Hash;
use App\User;
use App\Job;
use Validator;
use Illuminate\Support\Facades\Input;


class ApiController extends Controller
{
    public function register(){
		
		
				$first=Request::get('firstname');
$last=Request::get('lastname');
$phone=Request::get('phone');
	
$email=Request::get('email');

$pass=Request::get('password');
		$county=Request::get('county');
			$gender=Request::get('gender');
		
		
		 $rules = array(
          'firstname'             => 'required',   
        'lastname'             => 'required', 
        'phone'            => 'required',   
         'email'            => 'required', 
	'password'            => 'required',
	'county'            => 'required',
	'gender'            => 'required',
	
     );    
    
    $validator = Validator::make(Request::all(), $rules);
		
		
		
		
		
		if ($validator->fails()) {
      return Response::make(
	 ['message'=>'Validation Failed',
	 'errors'=>  $validator->errors()]
	 );

    } else {
			if (User::where('email', '=', Input::get('email'))->count() > 0) {
				$message="That user exists no duplicates allowed";
        	return response()->json($message);
            }
	
               $user = new User;
                $user->firstname     =$first;
                $user->lastname     = $last;
                $user->email    = $email;
                $user->password =Hash::make($pass);
                $user->phone =$phone;
                $level="level3";
                $user->role=$level;
                $user->county=$county;
                $user->gender=$gender;
                $user->save();

       
                   
        
        
		 
                $message=200;
                return response()->json($user,$message);
            }




                }

                       public function login()
                {
                      $email=Request::get('email');

                    $credentials = Request::only('email','password');

                $remember=(Request::has('remember')) ? true : false;
                if(Auth::attempt($credentials,$remember)){
                   $user = Auth::user();
                     if($user->role==1)
                    {
                    return Redirect::intended('admin');
                    }
                     if($user->role==2)
                    {
                    return Redirect::intended('subadmin');
                    }
                      if($user->role=='level3')
                    {
                    $message=200;
                return response()->json($message);
                    }

              }else{

                $message="Invalid login credentials please try again";
                return response()->json($message);
    
	}
			   
			   
			   
			   
    }
	
	public function profile($id){
	
		
				

		$profile=DB::table('users')->select('firstname','lastname','gender','county','phone')->where('id','=',$id)->get();
		
	 $message=200;
 	return response()->json($profile,$message);
		
		
		
	}
	
	
	
	public function update(){
		$id=Request::get('id');
				$first=Request::get('firstname');
$last=Request::get('lastname');
$phone=Request::get('phone');
	
$email=Request::get('email');

$pass=Request::get('password');
		$county=Request::get('county');
			$gender=Request::get('gender');
		
	
		  $data = Request::all();
		
    $profile= User::findOrFail($id);
		if($profile==false){
			$success="The user information was not found";
			
		}else{
			
			 $profile->update($data);
		$success="The profile was updated successfully";
				$message=200;
 	return response()->json($success,$message);
			
		}
		 
		
		
		
	}
	
	
	public function jobs(){
		
		$job=Job::select('id','title','companyname','image','location','price','jobtype','description')->orderby('id','desc')->get();
		
			
	 $message=200;
 	return response()->json($job,$message);
		
		
		
	}
	
	
	
	
	
	
	
	
}
