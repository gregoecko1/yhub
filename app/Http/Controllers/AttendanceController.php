<?php

namespace App\Http\Controllers;
//use Illuminate\Http\Request;

use App\Http\Requests;
use Request;
use Response;
use App\Attendance;
use Input;
use DB;
use App\Student;
use Validator;
use Session;

class AttendanceController extends Controller
{
	
	public function index(){
		
	$attendance=Attendance::select('bookingid','counter','updated_at')->orderBy('id','desc')->paginate(10);
		$count=Attendance::select('bookingid','counter','updated_at')->count();
		  return view('admin.attendance')->with('attendance',$attendance)->with('count',$count);
		
	}
	
	
	public function attendance(){
		
		$id=Request::get('bookingid');
		
	    
		
		 $rules = array(
          'bookingid'             => 'required'
        
     );    
    
     $validator = Validator::make(Request::all(), $rules);
		

		
		
		
		 if ($validator->fails()) {
      return Response::make(
	  ['message'=>'Validation Failed',
	  'errors'=>  $validator->errors()]
	  );

    } else {
			
	if (Attendance::where('bookingid', '=', $id)->count() > 0) {
		
		
			$message=200;
		$counter=1;
		Attendance::where('bookingid',$id)->increment('counter');
		Student::where('bookingid',$id)->increment('counter');
		  //Bus::where('imei',$imei)->update(['longitude'=> $longi,'latitude'=>$latitude]);
		
		
		// DB::table('attendances')->increment('counter',$counter);
        
		 
 	return  Response::make(
	  ['message'=>'Attendance  was recorded successfully',
	  'status'=>200]
	  );
}else{
		 $profile = new Attendance;
        $profile->bookingid   = $id;
			$counter=1;
		$profile->counter=$counter; 
        
       
    
       
                   
        $profile->save();
	
		Student::where('bookingid',$id)->increment('counter');
		$message=200;
		
		
		return  Response::make(
	  ['message'=>'New Attendace was recorded successfully',
	  'status'=>200,
	   'data'=>$profile
	  ]
	  );
		 }
		 }
			

		 
		
	
		
	}
	
	
    
}
