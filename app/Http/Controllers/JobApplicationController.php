<?php

namespace App\Http\Controllers;

use App\Jobapplication;
use Illuminate\Http\Request;

use App\Http\Requests;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\View;
use Auth;
use Alert;
use Validator;
use Illuminate\Validation;
use Illuminate\Support\Facades\Redirect;
use App\Job;
use Illuminate\Support\Facades\Session;

class JobApplicationController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
         return view('jobapplication');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $params = [
            'title' => 'user applying for a single job',
        ];
        return view('jobapplication')->with($params);
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
       // $all = Input::all();
       // dd($all)

        $user =Auth::user()->id;
        //dd($user);
        $apply = Jobapplication::create([
            'job_id' => $request->input('job_id'),
            'creator_id' => $request->input('creator_id'),
            'user_id' => $user,
            'amount' => $request->input('amount'),
            'time' => $request->input('time'),
            'message' => $request->input('message'),

        ]);

            if($file = $request->hasFile('resume')) {

                $file = $request->file('resume') ;


                $destinationPath = 'uploads'; // upload path

                $extension = Input::file('resume')->getClientOriginalExtension(); // getting image extension
                $fileName = rand(11111,99999).'.'.$extension; // renameing image
                Input::file('resume')->move($destinationPath, $fileName); // uploading file to given path

                // sending back with message
                $path = Input::file('resume')->getRealPath();
                $name = Input::file('resume')->getClientOriginalName();
                $destinationLink=$destinationPath.'/'.$fileName;
                $apply->resume = $destinationLink;

        }
        //dd($apply);
        $apply->save();

        Alert::success('Thanks fo appling!');

        return Redirect::to('job');
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }

    public  function test(Request $request){
        //dd($request);

    }
}
