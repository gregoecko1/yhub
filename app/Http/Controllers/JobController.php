<?php

namespace App\Http\Controllers;

use App\Category;
use Illuminate\Http\Request;
use DB;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Redirect;
use Illuminate\Validation;
use Validator;
use Auth;
use UxWeb\SweetAlert\SweetAlert;
use Alert;
use App\Http\Requests;
use App\Job;
use User;

class JobController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function __construct()
    {
        $this->middleware('auth');
    }

    public function index()
    {


        $job=Job::select('id','title','companyname','image','location','price','jobtype','description','creator_id')->orderby('id','desc')->paginate(6);
        return view('browsejob')->with('job',$job);
      
    }
	
	 /* public function jobs()
    {
        $job=Job::select('id','title','companyname','image','location','price','jobtype','description','status','bids','creator_id')->orderby('id','desc')->get();
        return view('admin.job')->with('job',$job);
    }*/
	
	 public function single($id){
    
		
		$job=Job::select('id','title','companyname','location','price','jobtype','description','creator_id')->where('id',$id)->get();
		 
		 
      	 return view('singlejob')->with('job',$job);
    }
    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        $cats = array(
            'title' =>  DB::table('categories')->get()
        );

        return view('createjob', $cats);

        /*$cat = Category::all();

        return view('createjob',compact('cat'));*/
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        $user =Auth::user()->id;
        $users =Auth::user()->email;
        //dd($user);
        $job = Job::create([
            'cat_id'=> $request->input('cat_id'),
            'title'=> $request->input('title'),
            'email' => $users,
            'companyname' => $request->input('companyname'),
            'user_id' => $user,
            'location' => $request->input('location'),
            'jobtype' => $request->input('jobtype'),
            'price'=> $request->input('price'),
            'description' => $request->input('description'),
            'applicationlink' => $request->input('applicationlink'),
            'deadline' => $request->input('deadline'),
            'tagline' => $request->input('tagline'),
            'website' => $request->input('website'),
            'twitter' => $request->input('twitter'),

        ]);

        if($file = $request->hasFile('image')) {

            $file = $request->file('image') ;
            $destinationPath = 'public/uploads'; // upload path


            $extension = Input::file('image')->getClientOriginalExtension(); // getting image extension
            $fileName = rand(11111, 99999) . '.' . $extension; // renameing image
            Input::file('image')->move($destinationPath, $fileName); // uploading file to given path

            // sending back with message
            $path = Input::file('image')->getRealPath();
            $name = Input::file('image')->getClientOriginalName();
            $destinationLink=$destinationPath.'/'.$fileName;
            $job->image = $destinationLink;
        }

       // dd($job);


             $job->save();

                 Alert::success('Your Job has been posted!');

                 return Redirect::to('job/show  ');

    }


    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function showjobs()
    {
      $id =Auth::user()->id;
        //$job = Job::all()->get();
      $job = DB::table('users')->join('jobs','users.id','=','jobs.user_id')->where('users.id','=',$id)->select('jobs.price','jobs.title','jobs.description','jobs.deadline','jobs.bids','jobs.id')->get();
       // dd($job);
        return view('managejob',compact('job'));
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $job = Job::findOrFail($id);
    /*    $cats = array(
            'title' =>  DB::table('categories')->get()
        );*/


        return view('job.editjob',compact('job'));

    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $job = Job::findOrFail($id);

        if(file_exists('public/uploads')){
            @unlink('public/uploads');
        }
        $job->delete();
        $jobos =  Job::all();
        return view('managejob',compact('jobos'));
    }

}
