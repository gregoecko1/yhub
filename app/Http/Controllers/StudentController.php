<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

use App\Http\Requests;
use DB;
use Redirect;
use Hash;
use App\Student;
use Validator;



class StudentController extends Controller
{
    
	public function kemi(){
		
		$stud=Student::select('user','bookingid','county','center','class','gender','counter','updated_at')->where('center','=','KEMI')->paginate(10);
		$count=Student::select('user','bookingid','county','center','class','gender')->where('center','=','KEMI')->count();
		return view('admin.students')->with('stud',$stud)->with('count',$count);
		
		
		
	}
	
	
	public function classtwo(){
		
		$stud=Student::select('user','bookingid','county','center','class','gender','counter','updated_at')->where('center','=','KU, Main Campus')->where('class','=','Week 1 - Classroom 2')->paginate(10);
		$count=Student::select('user','bookingid','county','center','class','gender')->where('center','=','KU, Main Campus')->where('class','=','Week 1 - Classroom 2')->count();
		return view('admin.students')->with('stud',$stud)->with('count',$count);
		
		
		
	}
	
	
	public function classthree(){
		
		$stud=Student::select('user','bookingid','county','center','class','gender','counter','updated_at')->where('center','=','KU, Main Campus')->where('class','=','Week 1 - Classroom 3')->paginate(10);
		
			$count=Student::select('user','bookingid','county','center','class','gender')->where('center','=','KU, Main Campus')->where('class','=','Week 1 - Classroom 3')->count();
		return view('admin.students')->with('stud',$stud)->with('count',$count);;
		
		
		
	}
	
	public function classfour(){
		
		$stud=Student::select('user','bookingid','county','center','class','gender','counter','updated_at')->where('center','=','KU, Main Campus')->where('class','=','Week 1 - Classroom 4')->paginate(10);
		$count=Student::select('user','bookingid','county','center','class','gender')->where('center','=','KU, Main Campus')->where('class','=','Week 1 - Classroom 4')->count();
		return view('admin.students')->with('stud',$stud)->with('count',$count);
		
		
		
	}
	
		public function classfive(){
		
		$stud=Student::select('user','bookingid','county','center','class','gender','counter','updated_at')->where('center','=','KU, Main Campus')->where('class','=','Week 1 - Classroom 5')->paginate(10);
				$count=Student::select('user','bookingid','county','center','class','gender')->where('center','=','KU, Main Campus')->where('class','=','Week 1 - Classroom 5')->count();
		return view('admin.students')->with('stud',$stud)->with('count',$count);
		
		
		
	}
	
	
		public function meru(){
		
		$stud=Student::select('user','bookingid','county','center','class','gender','counter','updated_at')->where('center','=','The Cooperative University of Kenya Meru')->paginate(10);
			$count=Student::select('user','bookingid','county','center','class','gender')->where('center','=','The Cooperative University of Kenya Meru')->count();
		return view('admin.students')->with('stud',$stud)->with('count',$count);
		
		
		
	}
	
		
		public function mombasa(){
		
		$stud=Student::select('user','bookingid','county','center','class','gender','counter','updated_at')->where('center','=','KU, Mombasa City Campus')->paginate(10);
				$count=Student::select('user','bookingid','county','center','class','gender')->where('center','=','KU, Mombasa City Campus')->count();
		return view('admin.students')->with('stud',$stud)->with('count',$count);
		
		
		
	}
	
			public function nakuru(){
		
		$stud=Student::select('user','bookingid','county','center','class','gender','counter','updated_at')->where('center','=','RVIST, Main Campus')->paginate(10);
				$count=Student::select('user','bookingid','county','center','class','gender')->where('center','=','RVIST, Main Campus')->count();
		return view('admin.students')->with('stud',$stud)->with('count',$count);
		
		
		
	}
	
	
	public function kisumu(){
		
		$stud=Student::select('user','bookingid','county','center','class','gender','counter','updated_at')->where('center','=','Tom Mboya Labour College')->paginate(10);
		$count=Student::select('user','bookingid','county','center','class','gender')->where('center','=','Tom Mboya Labour College')->count();
		return view('admin.students')->with('stud',$stud)->with('count',$count);
		
		
		
	}
	
	
}
