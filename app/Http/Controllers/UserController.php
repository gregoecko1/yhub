<?php

namespace App\Http\Controllers;
use Illuminate\Http\Request;
use Session;
use Auth;
use App\ActivationService;
use DB;
use Redirect;
use Hash;
use App\User;
use Illuminate\Support\Facades\Input;
use Illuminate\Support\Facades\Validator;
use Alert;


class UserController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    protected $activationService;

   public function __construct(ActivationService $activationService)
   {
       $this->activationService = $activationService;

   }


    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        //
    }
	
	public function register(){
	     $rules = array(
	    'lastname'             => 'required',
        'firstname'             => 'required', 
        'phone'            => 'required|numeric',   
        'email'            => 'required|email',   
		'county'            => 'required', 	
		'gender'            => 'required',
        'password' =>'required|numeric',
     );    
    
     $validator = Validator::make(Input::all(), $rules);


	  if ($validator->fails()) {
	     // dd($validator);
      return Redirect::back()->withInput()->withErrors($validator);

    } else {
	
if (User::where('email', '=', Input::get('email'))->count() > 0) {
 	return Redirect::to('register')->with('message', 'Your account already exists');
}else{
        $first=Input::get('firstname');
        $last=Input::get('lastname');
        $phone=Input::get('phone');

        $email=Input::get('email');

        $pass=Input::get('password');
        $county=Input::get('county');
        $gender=Input::get('gender');

        $user = new User;
        $user->firstname     =$first;
        $user->lastname     = $last;
        $user->email    = $email;
        $user->password =Hash::make($pass);
        $user->phone =$phone;
        $level="level3";
        $user->role=$level;
	    $user->county=$county;
	    $user->gender=$gender;
        $user->save();

    $this->activationService->sendActivationMail($user);
    return redirect('login')->with('status', 'We sent you an activation code. Check your email.');



    //return Redirect::intended('login');
}
	
	
		/* $thanks='Thank you for your registration.';
    $data = ['thanks'=>$thanks,'pass'=>$pass,'first'=>$first,'last'=>$last];

    
    Mail::send('regemail',$data, function($m) use($email){
        $m->to($email)->subject('Registration');
    });
	
	

	 $credentials = Input::only('email','password');

	 if (Auth::attempt($credentials)) {
		 
		
		return Redirect::to('dashboard');	 
			 
		
	      }
	

        }
	  }*/
	  }
	
}
	
	public function login(Request $request){

        $this->validate($request, [
            'email' => 'required|email', 'password' => 'required',
        ]);


            $email=Input::get('email');
		 
        $credentials = Input::only('email','password');

        $remember=(Input::has('remember')) ? true : false;
        if(Auth::attempt($credentials,$remember)){
           $user = Auth::user();
             if($user->role==1)
            {
            return Redirect::intended('admin');
            }
             if($user->role==2)
            {
            return Redirect::intended('subadmin');
            }
              if($user->role=='level3')
            {
                Alert::success('welcome back');
           return Redirect::intended('job');
            }
        
        }else{
            //$errors = [$this->username() => trans('auth.failed')];
            return redirect('login')
                ->withInput($request->only('email', 'remember'))
                ->withErrors([
                    'email' => 'Incorrect email address or password',
                ]);

           // return Redirect::back()->withErrors('email', '*invalid password or email*');

            //return Redirect::to('login')->with('errors', '*invalid password or email*');
	}
			   
    }
	
	
	public function logout(){

        auth()->logout();
        Session::flush();
	    return Redirect::to('/');

	}


    public function test(){
        Alert::message('bye!');

        return Redirect::to('/');

    }

    public function authenticated(Request $request, $user)
    {
        if (!$user->activated) {
            $this->activationService->sendActivationMail($user);
            auth()->logout();
            return back()->with('status', 'You need to confirm your account. We have sent you an activation code, please check your email.');
        }
        return redirect()->intended($this->redirectPath());
    }
    public function activateUser($token)
    {
        if ($user = $this->activationService->activateUser($token)) {
            auth()->login($user);
            return redirect($this->redirectPath());
        }
        abort(404);
    }




}
