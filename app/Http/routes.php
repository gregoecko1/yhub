<?php
use Illuminate\Support\Facades\Input;
use UxWeb\SweetAlert\SweetAlert;
use App\Job;
use App\Category;


/*
|--------------------------------------------------------------------------
| Application Routes
|--------------------------------------------------------------------------
|
| Here is where you can register all of the routes for an application.
| It's a breeze. Simply tell Laravel the URIs it should respond to
| and give it the controller to call when that URI is requested.
|
*/



Route::group(['middleware' => 'web'], function() {
    Route::resource('admin', 'AdminController');
    Route::get('hr',array('as' => 'hr', 'uses' => 'AdminController@hr'));
    Route::resource('category', 'CategoryController');
    Route::resource('client', 'ClientController');
    Route::resource('job', 'JobController');
    Route::resource('user', 'UserController');
    Route::resource('finance', 'FinanceController');
    Route::resource('notification', 'NotificationController');
    Route::resource('role', 'RoleController');

    Route::get('/', function () {
        //  Alert::message('Robots are working!');

        $job=Job::select('id','title','companyname','image','location','price','jobtype','description','creator_id')->orderby('id','desc')->paginate(5);
        return view('index')->with('job',$job);
    });

    /*Route::get('browsejobs', function () {
        return view('browsejob');
    });*/
    Route::get('browsejobs',array('as' => 'browsejobs', 'uses' => 'JobController@jobs'));
    Route::get('singlejob/{id}',array('as' => 'singlejob/{id}', 'uses' => 'JobController@single'));
    Route::get('singlejob', function () {
        return view('singlejob');
    });


    Route::get('browsecats', function () {
        $cats = Category::all();
        return view('browsecats',compact('cats'));
    });

    Route::post('testing',function(){

        $all=Input::all();
        dd($all);

    });

    /*Route::get('apply', function () {
        return view('jobapplication');
    });*/

    Route::resource('apply', 'JobApplicationController');

    Route::get('test', function (){

        Alert::message('You applied successfully!');

    });

    Route::get('register', function () {
        return view('register');
    });

    Route::get('profile', function () {
        return view('profile');
    });


    Route::post('registeruser',array('as' => 'registeruser', 'uses' => 'UserController@register'));
    Route::post('login',array('as' => 'login', 'uses' => 'UserController@login'));
   // Route::get('logout',array('as' => 'logout', 'uses' => 'UserController@logout'));
    Route::get('logout', 'UserController@logout');


    Route::get('testar',array('as' => 'testar', 'uses' => 'UserController@test'));
    Route::get('logins', function () {
        return view('login');
    });


});

Route::get('testjob',array('as' => 'testjob', 'uses' => 'JobController@showjobs'));


/****API***/
Route::post('api/v1/registration',array('as' => 'api/v1/registration','uses' => 'ApiController@register'));
Route::post('api/v1/login',array('as' => 'api/v1/login','uses' => 'ApiController@login'));
Route::get('api/v1/profile/{id}',array('as' => 'api/v1/profile/{id}','uses' => 'ApiController@profile'));
Route::get('api/v1/jobs',array('as' => 'api/v1/jobs','uses' => 'ApiController@jobs'));
Route::put('api/v1/updateprofile',array('as' => 'api/v1/updateprofile','uses' => 'ApiController@update'));
Route::post('api/v1/attendance',array('as' => 'api/v1/attendance','uses' => 'AttendanceController@attendance'));

Route::get('kemi',array('as' => 'classone', 'uses' => 'StudentController@kemi'));

Route::get('classtwo',array('as' => 'classtwo', 'uses' => 'StudentController@classtwo'));


Route::get('classthree',array('as' => 'classthree', 'uses' => 'StudentController@classthree'));

Route::get('classfour',array('as' => 'classfour', 'uses' => 'StudentController@classfour'));

Route::get('classfive',array('as' => 'classfive', 'uses' => 'StudentController@classfive'));

Route::get('meru',array('as' => 'meru', 'uses' => 'StudentController@meru'));

Route::get('mombasa',array('as' => 'mombasa', 'uses' => 'StudentController@mombasa'));

Route::get('nakuru',array('as' => 'nakuru', 'uses' => 'StudentController@nakuru'));

Route::get('kisumu',array('as' => 'kisumu', 'uses' => 'StudentController@kisumu'));
Route::get('attendance',array('as' => 'attendance', 'uses' => 'AttendanceController@index'));









Route::auth();
Route::get('user/activation/{token}', 'Auth\AuthController@activateUser')->name('user.activate');


Route::get('/home', 'HomeController@index');



Route::get('auth/{provider}', 'Auth\AuthController@redirectToProvider');
Route::get('auth/{provider}/callback', 'Auth\AuthController@handleProviderCallback');
