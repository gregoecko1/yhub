<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Job extends Model
{
    //
    protected $fillable= ['cat_id','user_id','creator_id','title','companyname','image','location',
                            'price','jobtype','description','status','bids','applicationlink','deadline','tagline','website','twitter','email'];


}
