<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Jobapplication extends Model
{
    protected $fillable = [
        'job_id','creator_id','user_id','amount','time','message','resume'
    ];
}
