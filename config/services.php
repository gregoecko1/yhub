<?php

return [

    /*
    |--------------------------------------------------------------------------
    | Third Party Services
    |--------------------------------------------------------------------------
    |
    | This file is for storing the credentials for third party services such
    | as Stripe, Mailgun, Mandrill, and others. This file provides a sane
    | default location for this type of information, allowing packages
    | to have a conventional place to find your various credentials.
    |
    */

    'mailgun' => [
        'domain' => env('MAILGUN_DOMAIN'),
        'secret' => env('MAILGUN_SECRET'),
    ],

    'ses' => [
        'key' => env('SES_KEY'),
        'secret' => env('SES_SECRET'),
        'region' => 'us-east-1',
    ],

    'sparkpost' => [
        'secret' => env('SPARKPOST_SECRET'),
    ],

    'stripe' => [
        'model' => App\User::class,
        'key' => env('STRIPE_KEY'),
        'secret' => env('STRIPE_SECRET'),
    ],
    'twitter' => [
        'client_id'     => '0rE6nftrZVltTYSHuKJnNiHkD',
        'client_secret' => 'dEbQLVEdsxmYDCgV5sMtwNtmeTS7M9VVmEL3V2wBFYq36o2o0V',
        'redirect'      => 'http://localhost:8080/yhub/job',
    ],

  /*    TWITTER_ID={{0rE6nftrZVltTYSHuKJnNiHkD}}
        TWITTER_SECRET={{dEbQLVEdsxmYDCgV5sMtwNtmeTS7M9VVmEL3V2wBFYq36o2o0V}}
        TWITTER_URL={{http://localhost:8080/yhub/job}}*/

];
