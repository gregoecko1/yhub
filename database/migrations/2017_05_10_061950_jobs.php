<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Jobs extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
             Schema::create('jobs', function (Blueprint $table) {
            $table->increments('id');
			$table->string('cat_id',100);
			$table->string('user_id',100);
		    $table->string('creator_id',100);
            $table->string('title',100);
			$table->string('companyname',100);
			$table->string('image',100);
            $table->string('location',200);
			$table->string('price',100);
			$table->string('jobtype',100);
			$table->text('description');
		    $table->string('status',12);
		    $table->string('applicationlink');
		    $table->string('deadline');
		    $table->string('tagline');
		    $table->string('website');
		    $table->string('twitter');
		    $table->string('email');

			$table->string('bids',100);
				 
				 
			
            $table->rememberToken();
            $table->timestamps();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('jobs');
    }
}
