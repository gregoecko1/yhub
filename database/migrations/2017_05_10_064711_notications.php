<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Notications extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
           Schema::create('notifications', function (Blueprint $table) {
            $table->increments('id');
			$table->string('user_id',12);
            $table->string('title',100);
			$table->text('description');
  
            $table->timestamps();
        });      
        

    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
         Schema::drop('notifications');
    }
}
