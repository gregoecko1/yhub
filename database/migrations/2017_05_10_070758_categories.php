<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Categories extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
		Schema::create('categories', function (Blueprint $table) {
            $table->increments('id');
			$table->string('user_id',12);
            $table->string('title',100);
            $table->string('name',100);

  
            $table->timestamps();
        });      
        
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
       
		Schema::drop('categories');
		
    }
}
