<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class Attendances extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
 public function up()
    {
         Schema::create('attendances',function(Blueprint $table){
            $table->increments('id');
			 $table->string('bookingid',150);
			 
            $table->timestamps();
            
        });       
    }
    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('attendances');
    }
}
