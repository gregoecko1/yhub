<?php

use Illuminate\Database\Schema\Blueprint;
use Illuminate\Database\Migrations\Migration;

class CreateJobapplicationsTable extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('jobapplications',function(Blueprint $table){
            $table->increments('id');
            $table->string('job_id',12);
            $table->string('creator_id',12);
            $table->string('user_id',12);
            $table->string('amount',100);
            $table->string('time',100);
            $table->string('message');
            $table->string('resume');
            $table->timestamps();

        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::drop('jobapplications');
    }
}
