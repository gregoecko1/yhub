<?php

use Illuminate\Database\Seeder;

class CategoriesTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        //
        DB::table('categories')->delete();
        $faker = Faker\Factory::create();



        DB::table('categories')->insert(
            array(
                array('title'=>'IT','name'=>'Mobile Development'),
                array('title'=>'IT','name'=>'QA & Testing'),
                array('title'=>'IT','name'=>'Web Development'),
                array('title'=>'IT','name'=>'Ecommerce Development'),
                array('title'=>'IT','name'=>'Game Development'),
                array('title'=>'IT','name'=>'Mobile Development'),
                array('title'=>'IT','name'=>'Desktop Software Development'),

        ));



    }
}
