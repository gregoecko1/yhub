<?php

use Illuminate\Database\Seeder;

class JobsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        
		$faker = Faker\Factory::create();



        $limit = 20;

        for ($i = 0; $i < $limit; $i++) {
            DB::table('jobs')->insert([ //,
				'cat_id' => $faker->randomDigit,
				'user_id' => $faker->randomDigit,
				'creator_id' => $faker->randomDigit,
                'title' => $faker->titleFemale,
                'companyname' => $faker->name,
                'image' => $faker->image('public/uploads',400,300, null, false),
				 'location' => $faker->city ,
                'price' => $faker->numberBetween($min = 1000, $max = 9000),
                'jobtype' => $faker->titleMale,
				 'description' => $faker->text($maxNbChars = 200),
                'status' => $faker->randomDigit,
				 'bids' => $faker->randomDigit
            ]);
        }
		
		
		
    }
}
