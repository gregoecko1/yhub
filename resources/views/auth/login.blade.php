@extends('includes.nav')

@section('content')
@include('includes.loginheader')



<div class="top-content">

    <div class="inner-bg">

        <div class="container">
            <div class="row">
                <div class="col-sm-8 col-sm-offset-2 text">
                    <h1><strong style="color:white;">Y Hub</strong> </h1>
                    <div class="description">


                    </div>
                </div>
            </div>
            <div class="row">
                <div class="col-sm-6 col-sm-offset-3 form-box">
                    <div class="form-top">
                        <div class="form-top-left">
                            <h3>Login to our site</h3>
{{--
                            <p>Enter your username and password to log on:</p>

--}}
                            <div class="form-group" style="">
                                <span class="errors" style="color:red;">
                            @if(session()->has('warning'))
                                {!! session()->get('warning') !!}
                            @endif
                            @if(session()->has('status'))
                                {!! session()->get('status') !!}
                            @endif
                                </span>
                            </div>
                        </div>
                        <div class="form-top-right">
                            <i class="fa fa-key"></i>
                        </div>
                    </div>
                    <div class="form-bottom">
                        {{Form::open(array('url'=>'login','class'=>'login-form'))}}

                        <div class="form-group">
                            <label class="sr-only" for="form-username">Username</label>
                            <input type="email" name="email" placeholder="email..." class="form-username form-control" id="form-username">
                            <span class="errors" style="color:red;"> {{ $errors->first('email')}}</span>

                        </div>
                        <div class="form-group">
                            <label class="sr-only" for="form-password">Password</label>
                            <input type="password" name="password" placeholder="Password..." class="form-password form-control" id="form-password">
                            <span class="errors" style="color:red;"> {{ $errors->first('password')}}</span>

                        </div>
                        <div class="form-group">
                            <input type="submit" value="Sign In" class="btn btn-success"/>
                            <p class="pull-right"><a href="{{URL::to('/password/reset')}}">Forgot your password?</a></p>

                        </div>

                        {{Form::close()}}
                    </div>
                </div>
            </div>
            <div class="row">
                <h5><a href="{{URL::to('register')}}">...Not registered click here to register:</a></h5>
                <div class="col-sm-6 col-sm-offset-3 social-login">
                    <h3>...or login with:</h3>
                    <div class="social-login-buttons">
                        <a class="btn btn-link-1 btn-link-1-facebook" href="#">
                            <i class="fa fa-facebook"></i> Facebook
                        </a>
                        <a href="{{ url('/auth/twitter') }}" class="btn btn-twitter"><i class="fa fa-twitter"></i> Twitter</a>

                        <a class="btn btn-link-1 btn-link-1-google-plus" href="#">
                            <i class="fa fa-google-plus"></i> Google Plus
                        </a>
                    </div>
                </div>
            </div>
        </div>
    </div>

</div>

@include('includes.loginfooter')
@endsection
