@extends('includes.nav')


@section('content')
    @include('includes.loginheader')
    <!-- Top content -->
    <header class="transparent sticky-header full-width">
        <div class="container">
            <div class="sixteen columns">

                <!-- Logo -->
                <div id="logo">
                    <h1><a href="index.html"><img src="images/logo.png" alt="Y Hub" /></a></h1>
                </div>

            @include('includes.nav')

            <!-- Navigation -->
                <div id="mobile-navigation">
                    <a href="#menu" class="menu-trigger"><i class="fa fa-reorder"></i> Menu</a>
                </div>

            </div>
        </div>
    </header>
    <div class="clearfix"></div>
    <div class="top-content">

        <div class="inner-bg">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 text">
                        <h1><strong style="color:white;">Y Hub</strong> </h1>

                        <div class="description">

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3 form-box">
                        <div class="form-top">
                            <div class="form-top-left">
                                <h3>Sign Up to our site</h3>
                    {{--            @if ( ::has('errors'))
                                    <center><h5 style="color:red; font-weight:bold; margin-left:20px;">{{Session::get('errors') }}</h5></center>
                                @endif--}}

                            </div>
                            <div class="form-top-right">
                                <i class="fa fa-key"></i>
                            </div>
                        </div>
                        <div class="form-bottom">
                            {{Form::open(array('url'=>'registeruser','class'=>'login-form'))}}

                            <div class="form-group">
                                <label class="sr-only" for="form-username">First Name</label>
                                <input type="text" name="firstname" placeholder="First Name..." class="form-username form-control" id="">
                                <span class="errors" style="color:red;"> {{ $errors->first('firstname')}}</span>

                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-username">Last Name</label>
                                <input type="text" name="lastname" placeholder="Last Name..." class="form-username form-control" id="-username">
                                <span class="errors" style="color:red;"> {{ $errors->first('lastname')}}</span>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-username">email</label>
                                <input type="email" name="email" placeholder="Email..." class="form-username form-control" id="form-ername">
                                <span class="errors" style="color:red;"> {{ $errors->first('email')}}</span>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-username">password</label>
                                <input type="password" name="password" placeholder="Password..." class="form-username form-control" id="form-username">
                                <span class="errors" style="color:red;"> {{ $errors->first('password')}}</span>
                            </div>

                            <div class="form-group">
                                <label class="sr-only" for="form-username">Phone Number</label>
                                <input type="text" name="phone" placeholder="Phone Number..." class="form-username form-control" id="-sername">
                                <span class="errors" style="color:red;"> {{ $errors->first('phone')}}</span>
                            </div>
                            <div class="form-group"><h5>Gender</h5></div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" value="female" id="exampleRadios1"  name="gender">
                                    Female
                                </label>
                                <span class="errors" style="color:red;"> {{ $errors->first('gender')}}</span>
                            </div>
                            <div class="form-check">
                                <label class="form-check-label">
                                    <input class="form-check-input" type="checkbox" value="male" name="gender">
                                    Male
                                </label>
                                <span class="errors" style="color:red;"> {{ $errors->first('gender')}}</span>
                            </div>
                            <div class="form-group">
                                <label for="exampleSelect2">County</label>
                                <select Baringo County="form-control" id="exampleSelect2" name="county">
                                    <option> Baringo County</option>
                                    <option>  Bomet County</option>
                                    <option>  Bungoma County</option>
                                    <option>  Busia County</option>
                                    <option>  Elgeyo Marakwet County</option>
                                    <option>  Embu County</option>
                                    <option> Garissa County</option>
                                    <option> Homa Bay County</option>
                                    <option>  Isiolo County</option>
                                    <option>  Kajiado County</option>
                                    <option>  Kakamega County</option>
                                    <option>  Kericho County</option>
                                    <option> Kiambu County</option>
                                    <option> Kilifi County</option>
                                    <option>  Kirinyaga County</option>
                                    <option>  Kisii County</option>
                                    <option> Kisumu County</option>
                                    <option> Kitui County</option>
                                    <option> Kwale County</option>
                                    <option>  Laikipia County</option>
                                    <option>  Lamu County</option>
                                    <option>  Machakos County</option>
                                    <option>  Makueni County</option>
                                    <option> Mandera County</option>
                                    <option>  Meru County</option>
                                    <option>  Migori County</option>
                                    <option> Marsabit County</option>
                                    <option> Mombasa County</option>
                                    <option> Muranga County</option>
                                    <option> Nairobi County</option>
                                    <option>  Nakuru County</option>
                                    <option>  Nandi County</option>
                                    <option>  Narok County</option>
                                    <option> Nyamira County</option>
                                    <option> Nyandarua County</option>
                                    <option>  Nyeri County</option>
                                    <option>  Samburu County</option>
                                    <option> Siaya County</option>
                                    <option> Taita Taveta County</option>
                                    <option> Tana River County</option>
                                    <option> Tharaka Nithi County</option>
                                    <option>Trans Nzoia County</option>
                                    <option>Turkana County</option>
                                    <option>Uasin Gishu County</option>
                                    <option>Vihiga County</option>
                                    <option>Wajir County</option>
                                    <option>West Pokot County</option>

                                </select>
                            </div>
                            <button class="btn btn-success" type="submit">Sign Up!</button>

                            {{Form::close()}}
                        </div>
                    </div>
                </div>
                <div class="row">
                    <h5><a href="{{URL::to('logins')}}">...You have account click here to Login:</a></h5>
                    <div class="col-sm-6 col-sm-offset-3 social-login">
                        <h3>...or login with:</h3>
                        <div class="social-login-buttons">
                            <a class="btn btn-link-1 btn-link-1-facebook" href="#">
                                <i class="fa fa-facebook"></i> Facebook
                            </a>

                            <a href="{{ url('/auth/twitter') }}" class="btn btn-twitter"><i class="fa fa-twitter"></i> Twitter</a>

                            </a>
                            <a class="btn btn-link-1 btn-link-1-google-plus" href="#">
                                <i class="fa fa-google-plus"></i> Google Plus
                            </a>
                        </div>
                    </div>
                </div>
            </div>
        </div>

    </div>

    @include('includes.loginfooter')
@endsection
