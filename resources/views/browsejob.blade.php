@include('includes.header')

<body>
<div id="wrapper">

@include('includes.nav2')


<!-- Titlebar
================================================== -->
<div id="titlebar"  style="margin-top:15px;">
	<div class="container">
		<div class="ten columns">
			<!--<span>We found 1,412 jobs matching:</span>-->

		</div>

		<div class="six columns">
			<!--<a href="add-job.html" class="button">Post a Job, It’s Free!</a>-->
		</div>

	</div>
</div>


<!-- Content
================================================== -->
<div class="container">
	<!-- Recent Jobs -->
	<div class="eleven columns">
	<div class="padding-right">
		
		<form action="#" method="get" class="list-search">
			<button><i class="fa fa-search"></i></button>
			<input type="text" placeholder="job title, keywords or company name" value=""/>
			<div class="clearfix"></div>
		</form>

		<ul class="job-list full">

			@foreach($job as $jobs)
			<li><a href="{{ URL::to('singlejob')}}/{{$jobs->id}}">
					<img src="{{ $jobs->image}}"> </img>

				<div class="job-list-content">
					<h4>{{$jobs->title}} <span class="full-time">{{$jobs->jobtype}}</span></h4>
					<div class="job-icons">
						<span><i class="fa fa-briefcase"></i> {{$jobs->companyname}}</span>
						<span><i class="fa fa-map-marker"></i> {{$jobs->location}}</span>
						<span><i class="fa fa-money"></i>{{$jobs->price}}</span>
					</div>
					<p>{{$jobs->description}}</p>
				</div>
				</a>
				<div class="clearfix"></div>
			</li>
			@endforeach

			
	

		
		</ul>
		<div class="clearfix"></div>

		<div class="pagination-container">
		
			<nav class="pagination">
					{{$job->render() }}
				
				<!--<ul>
					<li><a href="#" class="current-page">1</a></li>
					<li><a href="#">2</a></li>
					<li><a href="#">3</a></li>
					<li class="blank">...</li>
					<li><a href="#">22</a></li>
				</ul>-->
			</nav>

			<!--<nav class="pagination-next-prev">
				<ul>
					<li><a href="#" class="prev">Previous</a></li>
					<li><a href="#" class="next">Next</a></li>
				</ul>
			</nav>-->
		</div>

	</div>
	</div>


	<!-- Widgets -->
	<div class="five columns">

		<!-- Sort by -->
		<div class="widget">
			<h4>Sort by</h4>

			<!-- Select -->
			<select data-placeholder="Choose Category" class="chosen-select-no-single">
				<option selected="selected" value="recent">Newest</option>
				<option value="oldest">Oldest</option>
				<option value="expiry">Expiring Soon</option>
				<option value="ratehigh">Hourly Rate – Highest First</option>
				<option value="ratelow">Hourly Rate – Lowest First</option>
			</select>

		</div>

		

		<!-- Job Type -->
		<div class="widget">
			<h4>Job Type</h4>

			<ul class="checkboxes">
				<li>
					<input id="check-1" type="checkbox" name="check" value="check-1" checked>
					<label for="check-1">Accounts</label>
				</li>
				<li>
					<input id="check-2" type="checkbox" name="check" value="check-2">
					<label for="check-2">Web Designing <span>(312)</span></label>
				</li>
				<li>
					<input id="check-3" type="checkbox" name="check" value="check-3">
					<label for="check-3"> Music Production<span>(269)</span></label>
				</li>
				<li>
					<input id="check-4" type="checkbox" name="check" value="check-4">
					<label for="check-4">Internship <span>(46)</span></label>
				</li>
				<li>
					<input id="check-5" type="checkbox" name="check" value="check-5">
					<label for="check-5">Freelance <span>(119)</span></label>
				</li>
			</ul>

		</div>



	</div>
	<!-- Widgets / End -->


</div>


@include('includes.footer')