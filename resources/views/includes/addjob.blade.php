<!DOCTYPE html>
<!--[if IE 8 ]><html class="ie ie8" lang="en"> <![endif]-->
<!--[if (gte IE 9)|!(IE)]><!--><html lang="en"> <!--<![endif]-->

<!-- Mirrored from www.vasterad.com/themes/workscout/add-job.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 20 Apr 2017 13:46:06 GMT -->
<head>

    <!-- Basic Page Needs
    ================================================== -->
    <meta charset="utf-8">
    <title>Work Scout</title>
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <!-- Mobile Specific Metas
    ================================================== -->
    <meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

    <!-- CSS
    ================================================== -->
    {!!Html::style('css/style.css')!!}
    {!!Html::style('css/color/green.css')!!}
    {!!Html::style('assets/sweet-alert/sweet-alert.min.css')!!}
   {{-- <link rel="stylesheet" href="css/style.css">--}}
    <link rel="stylesheet" href="css/colors/green.css" id="colors">



    <!--[if lt IE 9]>
    <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

</head>
<body>
<div id="wrapper">


    <!-- Header
    ================================================== -->
    <header class="sticky-header">
        <div class="container">
            <div class="sixteen columns">

                <!-- Logo -->
                <div id="logo">
                    <h1><a href="{{URL::to('/')}}">
                            {!!Html::Image('images/logo.png')!!} </a></h1>
                </div>

                <!-- Menu -->
                <nav id="navigation" class="menu">
                    <ul id="responsive">

                        <li><a href="index.html" id="current">Home</a></li>
                        <li><a href="add-job.html">Add Job</a></li>
                        <li><a href="manage-jobs.html">Manage Jobs</a></li>
                        <li><a href="manage-applications.html">Manage Applications</a></li>
                        <li><a href="browse-resumes.html">Browse Resumes</a></li>


                    </ul>
                    <ul class="float-right">

                        <li class="dropdown">
                        @if (Auth::guest())
                            <li><a href="{{ url('/login') }}">Login</a></li>
                            <li><a href="{{ url('/register') }}">Register</a></li>
                        @else
                            <li class="dropdown">
                                <a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
                                    {{ Auth::user()->firstname }} <span class="caret"></span>
                                </a>

                                <ul class="dropdown-menu">
                                    <li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
                                    <li><a href="{{ url::to('job') }}"><i class="fa fa-btn fa-plus-circle"></i>Apply for Job</a></li>
                                </ul>
                            </li>
                            @endif
                            </li>


                    </ul>




                </nav>

                <!-- Navigation -->

                <div id="mobile-navigation">
                    <a href="#menu" class="menu-trigger"><i class="fa fa-reorder"></i> Menu</a>
                </div>

            </div>
        </div>
    </header>
    <div class="clearfix"></div>


    <!-- Titlebar
    ================================================== -->
    <div id="titlebar" class="single submit-page">
        <div class="container">



        </div>
    </div>



