

<!-- Footer
================================================== -->
<div class="margin-top-15"></div>

<div id="footer">
	<!-- Main -->
	<div class="container">

		<div class="seven columns">
			<h4>About</h4>
			<p>Y Hub is a youths hub equiped with online jobs,online courses and training ready to challange the lives of young people with potential in the society</p>
			<a href="mu-account.html" class="button">Get Started</a>
		</div>

		<div class="three columns">
			<h4>Company</h4>
			<ul class="footer-links">
				<li><a href="#">About Us</a></li>
				<li><a href="#">Careers</a></li>
				<li><a href="#">Our Blog</a></li>
				<li><a href="#">Terms of Service</a></li>
				<li><a href="#">Privacy Policy</a></li>
				<li><a href="#">Hiring Hub</a></li>
			</ul>
		</div>
		
		<div class="three columns">
			<h4>Press</h4>
			<ul class="footer-links">
				<li><a href="#">In the News</a></li>
				<li><a href="#">Press Releases</a></li>
				<li><a href="#">Awards</a></li>
				<li><a href="#">Testimonials</a></li>
				<li><a href="#">Timeline</a></li>
			</ul>
		</div>		

		<div class="three columns">
			<h4>Browse</h4>
			<ul class="footer-links">
				<li><a href="#">Freelancers by Category</a></li>
				<li><a href="#">Freelancers in USA</a></li>
				<li><a href="#">Freelancers in UK</a></li>
				<li><a href="#">Freelancers in Canada</a></li>
				<li><a href="#">Freelancers in Australia</a></li>
				<li><a href="#">Find Jobs</a></li>

			</ul>
		</div>

	</div>

	<!-- Bottom -->
	<div class="container">
		<div class="footer-bottom">
			<div class="sixteen columns">
				<h4>Follow Us</h4>
				<ul class="social-icons">
					<li><a class="facebook" href="#"><i class="icon-facebook"></i></a></li>
					<li><a class="twitter" href="#"><i class="icon-twitter"></i></a></li>
					<li><a class="gplus" href="#"><i class="icon-gplus"></i></a></li>
					<li><a class="linkedin" href="#"><i class="icon-linkedin"></i></a></li>
				</ul>
				<div class="copyrights">©  Copyright {{ date('Y')}} by <a href="#">Y Hub</a>. All Rights Reserved.</div>
			</div>
		</div>
	</div>

</div>

<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>

</div>
<!-- Wrapper / End -->


<!-- Scripts
================================================== -->
 
{!!Html::script('scripts/jquery-2.1.3.min.js')!!}
 
{!!Html::script('scripts/custom.js')!!} 
{!!Html::script('scripts/jquery.superfish.js')!!} 
{!!Html::script('scripts/jquery.themepunch.tools.min.js')!!} 
{!!Html::script('scripts/jquery.themepunch.revolution.min.js')!!} 
{!!Html::script('scripts/jquery.themepunch.showbizpro.min.js')!!} 
{!!Html::script('scripts/jquery.flexslider-min.js')!!} 
{!!Html::script('scripts/chosen.jquery.min.js')!!} 
{!!Html::script('scripts/jquery.magnific-popup.min.js')!!} 
{!!Html::script('scripts/waypoints.min.js')!!} 

{!!Html::script('scripts/jquery.counterup.min.js')!!}
{!!Html::script('scripts/jquery.jpanelmenu.js')!!} 
{!!Html::script('scripts/stacktable.js')!!} 
{!!Html::script('scripts/headroom.min.js')!!} 
{!!Html::script('assets/sweet-alert/sweet-alert.min.js')!!}
{!!Html::script('assets/sweet-alert/sweet-init.js')!!}
@include('sweet::alert');

</body>

<!-- Mirrored from www.vasterad.com/themes/workscout/index.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 20 Apr 2017 13:44:45 GMT -->
</html>