<!-- Header
================================================== -->
<header class="sticky-header">
    <div class="container">
        <div class="sixteen columns">

            <!-- Logo -->
            <div id="logo">
                <h1><a href="index.html"><img src="images/logo.png" alt="Work Scout" /></a></h1>
            </div>

            <!-- Menu -->
            <nav id="navigation" class="menu">
                <ul id="responsive">

                    <li><a href="index.html" id="current">Home</a>
                    </li>

                    <li><a href="browse-jobs.html">Job Search</a>

                        <ul>
                            <li><a href="browse-jobs.html">Browse Jobs</a></li>
                            <li><a href="browse-categories.html">Browse Categories</a></li>
                            <li><a href="add-resume.html">Add Resume</a></li>
                            <li><a href="manage-resumes.html">Manage Resumes</a></li>
                            <li><a href="job-alerts.html">Job Alerts</a></li>
                        </ul>
                    </li>

                    <li><a href="add-job.html">Post Job</a>
                        <ul>
                            <li><a href="add-job.html">Add Job</a></li>
                            <li><a href="manage-jobs.html">Manage Jobs</a></li>
                            <li><a href="manage-applications.html">Manage Applications</a></li>
                            <li><a href="browse-resumes.html">Browse Resumes</a></li>
                        </ul>
                    </li>

                    <li><a href="blog.html">Blog</a></li>
                </ul>


                <ul class="float-right">
                    <li><a href="my-account.html#tab2"><i class="fa fa-user"></i> Sign Up</a></li>
                    <li><a href="my-account.html"><i class="fa fa-lock"></i> Log In</a></li>
                </ul>

            </nav>

            <!-- Navigation -->

            <div id="mobile-navigation">
                <a href="#menu" class="menu-trigger"><i class="fa fa-reorder"></i> Menu</a>
            </div>

        </div>
    </div>
</header>
<div class="clearfix"></div>