
<header class="transparent sticky-header full-width">
<div class="container">
	<div class="sixteen columns">
	
		<!-- Logo -->
		<div id="logo">
			<h1><a href="{{URL::to('/')}}">
					{!!Html::Image('images/logo.png')!!} </a></h1>
		</div>

		<!-- Menu -->
		<nav id="navigation" class="menu">
			<ul id="responsive">

				<li><a href="{{URL::to('/')}}" id="current">Home</a>
				</li>

				<li><a href="">Job Search</a>
				
					<ul>
						<li><a href="{{URL::to('browsejobs')}}">Browse Jobs</a></li>
						<li><a href="{{URL::to('browsecats')}}">Browse Categories</a></li>
						<li><a href="add-resume.html">Add Resume</a></li>
						<li><a href="manage-resumes.html">Manage Resumes</a></li>
						<li><a href="job-alerts.html">Job Alerts</a></li>
					</ul>
				</li>

			

				<li><a href="blog.html">Blog</a></li>
			</ul>


			<ul class="float-right">

				<li><a href="{{URL::to('profile')}}"><i class="fa fa-user"></i>Profile</a></li>
				<li class="dropdown">
				@if (Auth::guest())
					<li><a href="{{ url('/login') }}">Login</a></li>
					<li><a href="{{ url('/register') }}">Register</a></li>
				@else
					<li class="dropdown">
						<a href="#" class="dropdown-toggle" data-toggle="dropdown" role="button" aria-expanded="false">
							{{ Auth::user()->firstname }} <span class="caret"></span>
						</a>

						<ul class="dropdown-menu">
							<li><a href="{{ url('/logout') }}"><i class="fa fa-btn fa-sign-out"></i>Logout</a></li>
							<li><a href="{{ url::to('job/create') }}"><i class="fa fa-btn fa-plus-circle"></i>Post Job</a></li>
						</ul>
					</li>
					@endif
				</li>
					
			
			</ul>

		</nav>

		<!-- Navigation -->
		<div id="mobile-navigation">
			<a href="#menu" class="menu-trigger"><i class="fa fa-reorder"></i> Menu</a>
		</div>

	</div>
</div>
</header>
<div class="clearfix"></div>


