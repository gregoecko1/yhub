@include('includes.addjob')


<!-- Content
    ================================================== -->
<div class="container">

    <!-- Submit Page -->
    <div class="sixteen columns">
        <div class="submit-page">
        {{Form::model($job,array('route'=>array('job.update',$job->id),'method'=>'post', 'files'=>'true'))}}

        <!-- Email -->
            <div class="form">
                <h5>Your Email</h5>
                <input class="search-field" type="text" name="email" value="{{$job->email}}" id="email" placeholder="mail@example.com" />
                <span class="errors" style="color:red;"> {{ $errors->first('email')}}</span>

            </div>

            <!-- Title -->
            <div class="form">
                <h5>Job Title</h5>
                <input class="search-field" type="text" placeholder="" name="title" id="title" value="{{$job->title}}"/>
                <span class="errors" style="color:red;"> {{ $errors->first('title')}}</span>

            </div>

            <!-- Location -->
            <div class="form">
                <h5>Location <span>(optional)</span></h5>
                <input class="search-field" type="text" name="location" placeholder="e.g. London" value="{{$job->location}}"/>
                <p class="note">Leave this blank if the location is not important</p>
                <span class="errors" style="color:red;"> {{ $errors->first('location')}}</span>

            </div>
            anthony@codigital.co.ke

            <!-- Job Type -->
            <div class="form">
                <h5>Job Type</h5>
                <select data-placeholder="Full-Time" name="jobtype" value="{{$job->jobtype}}" class="chosen-select-no-single">
                    <option value="Full-Time">Full-Time</option>
                    <option value="Part-Time">Part-Time</option>
                    <option value="Internship">Internship</option>
                    <option value="Freelance">Freelance</option>
                </select>
            </div>


            <!-- Choose Category -->
        {{--    <div class="form">
                <div class="select">
                    <h5>Category</h5>
                    <select data-placeholder="Choose Categories" name="cat_id" id="cat_id"  class="chosen-select" multiple>
                        @foreach($job as $jobs)

                            <option value="{{$jobs->cat_id}}">{{$jobs->cat_id}}</option>
                        @endforeach
                    </select>

                </div>
            </div>--}}

            <!-- Tags -->
            <div class="form">
                <h5>Budget <span></span></h5>
                <input class="search-field" type="text" name="price" value="{{$job->price}}" placeholder="e.g. 1000ksh or 200-500ksh" value=""/>
                <span class="errors" style="color:red;"> {{ $errors->first('price')}}</span>

            </div>


            <!-- Description -->
            <div class="form">
                <h5>Description</h5>
                <textarea class="WYSIWYG" name="description" value="{{$job->description}}" cols="40" rows="3" id="summary" spellcheck="true"></textarea>
            </div>

            <!-- Application email/url -->
            <div class="form">
                <h5>Application Email / URL</h5>
                <input type="text" name="applicationlink" value="{{ $job->applicationlink}}" placeholder="Enter an email address or website URL"/>
            </div>

            <!-- TClosing Date -->
            <div class="form">
                <h5>Closing Date <span>(optional)</span></h5>
                <input data-role="date"  name="deadline" value="{{$job->deadline}}" type="text" placeholder="yyyy-mm-dd">
                <p class="note">Deadline for new applicants.</p>
            </div>


            <!-- Company Details -->
            <div class="divider"><h3>Company Details</h3></div>

            <!-- Company Name -->
            <div class="form">
                <h5>Company Name</h5>
                <input type="text" name="companyname" value="{{$job->companyname}}" placeholder="Enter the name of the company">
            </div>

            <!-- Website -->
            <div class="form">
                <h5>Website <span>(optional)</span></h5>
                <input type="text" name="website" value="{{$job->website}}" placeholder="http://">
            </div>

            <!-- Teagline -->
            <div class="form">
                <h5>Tagline <span>(optional)</span></h5>
                <input type="text" name="tagline" value="{{$job->tagline}}" placeholder="Briefly describe your company">
            </div>

            <!-- Twitter -->
            <div class="form">
                <h5>Twitter Username <span>(optional)</span></h5>
                <input type="text" name="twitter" value="{{$job->twitter}}" placeholder="@yourcompany">
            </div>

            <!-- Logo -->
            <div class="form">
                <h5>Logo <span>(optional)</span></h5>
                <label class="upload-btn">
                    <input type="file" name="image" value="{{$job->image}}" multiple />
                    <i class="fa fa-upload"></i> Browse
                </label>
                <span class="fake-input">No file selected</span>
            </div>
            <div class="form-group">
                <input type="submit" class="btn btn-primary"  value="submit">
            </div>
            {{Form::close()}}

        </div>
    </div>

</div>


<!-- Footer
================================================== -->
<div class="margin-top-60"></div>

@include('includes.footer')

<!-- Back To Top Button -->
<div id="backtotop"><a href="#"></a></div>

</div>
<!-- Wrapper / End -->


<!-- Scripts
================================================== -->


</body>

<!-- Mirrored from www.vasterad.com/themes/workscout/add-job.html by HTTrack Website Copier/3.x [XR&CO'2014], Thu, 20 Apr 2017 13:46:07 GMT -->
</html>