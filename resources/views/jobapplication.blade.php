@include('includes.header')

<body>
<div id="wrapper">

@include('includes.nav2')


<!-- Titlebar
================================================== -->
    <div id="titlebar" style="margin-top:15px;">
        <div class="container">
            <div class="ten columns">
                <!--<span>We found 1,412 jobs matching:</span>-->
                <!--********************************************************** here am suposed to post the category of the job******************************-->
                <h4>Web, Software & IT</h4>
            </div>

            <div class="six columns">
                <!--<a href="add-job.html" class="button">Post a Job, It’s Free!</a>-->
            </div>

        </div>
    </div>


    <!-- Content
    ================================================== -->
    <div class="container">
        <!-- Recent Jobs -->
        <div class="eleven columns">
            <div class="padding-right">
<div id="small-dialog" class="">
    <div class="small-dialog-content">
        <div class="small-dialog-headline">
            <h2>Apply For This Job</h2>
        </div>
        {{Form::open(array('url'=>'testing','class'=>''))}}
            <input type="text" name="fullname" id="fullname" value="{{old('fullname')}}"  placeholder="amount"/>
            <input type="text"  name="email" id="email" value="{{old('email')}}" placeholder="Time" />
            <textarea  name="message" value="{{old('message')}}" id="message" placeholder="Your message / cover letter sent to the employer"></textarea>

            <!-- Upload CV -->
            <div class="upload-info"><strong>Upload your CV (optional)</strong> <span>Max. file size: 5MB</span></div>
            <div class="clearfix"></div>

            <label class="upload-btn">
                <input type="file" name="resume" id="resume" value="{{old('resume')}}" multiple />
                <i class="fa fa-upload"></i> Browse
            </label>
            <span class="fake-input">No file selected</span>

            <div class="divider"></div>

            <button type="submit" class="submit send">Send Application</button>
        {{Form::close()}}

    </div>

</div>
                </div>
            </div>
        </div>
    </div>
@include('includes.footer')