@include('includes.addjob')


<!-- Titlebar
================================================== -->
<div id="titlebar" class="single">
    <div class="container">

        <div class="sixteen columns">
            <h2>Manage Jobs</h2>
            <nav id="breadcrumbs">
                <ul>
                    <li>You are here:</li>
                    <li><a href="#">Home</a></li>
                    <li>Job Dashboard</li>
                </ul>
            </nav>
        </div>

    </div>
</div>


<!-- Content
================================================== -->
<div class="container">

    <!-- Table -->
    <div class="sixteen columns">

        <p class="margin-bottom-25">Your listings are shown in the table below. Expired listings will be automatically removed after 30 days.</p>

        <table class="manage-table responsive-table">

            <tr>
                <th><i class="fa fa-file-text"></i> Title</th>
                <th><i class="fa fa-check-square-o"></i>Description</th>
                <th><i class="fa fa-calendar"></i> Budget </th>
                <th><i class="fa fa-calendar"></i> Deadline</th>
                <th><i class="fa fa-user"></i>Bids</th>
                <th></th>
            </tr>

            <!-- Item #1 -->
            @foreach($job as $jobs)
            <tr>
              <td class="title">{{$jobs->title}}</td>

                <td class="centered">{{$jobs->description}}</td>

                <td>{{$jobs->price}}</td>
                <td>{{$jobs->deadline}}</td>
                <td></td>{{$jobs->bids}}</td>
                <td class="action">
                <a><i class="fa fa-pencil"></i>{!! link_to_route('job.edit','Edit',array($jobs->id),array('class'=>'btn btn-primary btn-sm')) !!} </a>
                <a><i class="fa fa-remove"></i>{!! Form::open(['method' => 'DELETE', 'route' => ['job.destroy', $jobs->id]]) !!}
                    {!! Form::submit('Delete',['class'=>'btn btn-danger btn-sm']) !!}
                    {!! Form::close() !!}
                </a>

                </td>
            </tr>
           @endforeach

        </table>

        <br>
        <a href="{{ url::to('job/create') }}" class="button">Add Job</a>


    </div>

</div>

@include('includes.footer')