@include('includes.header')

<body>
<div id="wrapper">

@include('includes.nav2')


    <div class="top-content">

        <div class="inner-bg">
            <div class="container">
                <div class="row">
                    <div class="col-sm-8 col-sm-offset-2 text">
                        <h1><strong style="color:white;">Y Hub</strong> </h1>
                        <div class="description">

                        </div>
                    </div>
                </div>
                <div class="row">
                    <div class="col-sm-6 col-sm-offset-3 form-box">
                        <div class="form-top">

                            <div class="form-top-right">
                                <i class="fa fa-key"></i>
                            </div>
                        </div>
                        <div class="form-bottom">
                            <div class="form-top-left">
                                <h3>Create Profile</h3>

                            </div>
                            {{Form::open(array('url'=>'registeruser','class'=>'login-form'))}}

                            <div class="form-group">
                                <label class="sr-only" for="form-username">Education Level</label>
                                <input type="text" name="firstname" placeholder="First Name..." class="form-username form-control" id="form-">
                                <span class="errors" style="color:red;"> {{ $errors->first('firstname')}}</span>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-username">Course </label>
                                <input type="text" name="lastname" placeholder="Last Name..." class="form-username form-control" id="-username">
                                <span class="errors" style="color:red;"> {{ $errors->first('lastname')}}</span>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-username">Skills</label>
                                <input type="email" name="email" placeholder="Email..." class="form-username form-control" id="form-ername">
                                <span class="errors" style="color:red;"> {{ $errors->first('email')}}</span>
                            </div>

                            <div class="form-group">
                                <label for="exampleSelect2"> How many years of experience from the skills mentioned above</label>
                                <select Baringo County="form-control" id="exampleSelect2" name="county">
                                    <option>Less than 1yr</option>
                                    <option>  1-3urs</option>
                                    <option>  4-8yrs</option>
                                    <option> more than 10yrs</option>

                                </select>
                            </div>
                            <div class="form-group">
                                <label class="sr-only" for="form-username">Profile photo</label>
                                <input type="file" name="file" placeholder="upload photo" class="form-username form-control" id="photo">
                                <span class="errors" style="color:red;"> {{ $errors->first('photo')}}</span>
                            </div>
                            <div class="form-group">
                            <button class="btn btn-success" type="submit">Submit!</button>
                            </div>

                            {{Form::close()}}
                        </div>
                    </div>
                </div>

            </div>
        </div>

    </div>

@include('includes.footer')