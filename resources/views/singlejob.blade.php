@include('includes.header')

<body>
<div id="wrapper">

@include('includes.nav2')


<!-- Titlebar
================================================== -->
<div id="titlebar" style="margin-top:15px;">
	<div class="container">
		<div class="ten columns">
			<!--<span>We found 1,412 jobs matching:</span>-->
			<!--********************************************************** here am suposed to post the category of the job******************************-->
			<h2>Web, Software & IT</h2>
		</div>

		<div class="six columns">
			<!--<a href="add-job.html" class="button">Post a Job, It’s Free!</a>-->
		</div>

	</div>
</div>


<!-- Content
================================================== -->
<div class="container">
	<!-- Recent Jobs -->
	<div class="eleven columns">
	<div class="padding-right">
		@foreach($job as $jobs)
			
				<div class="job-list-content">
					<h1 style="text-transform:capitalize;">{{$jobs->title}} &nbsp;<small> {{$jobs->jobtype}} </small></h1>
					<hr>
					<div class="job-icons">
						<span><i class="fa fa-briefcase"></i> {{$jobs->companyname}}</span>
						<span><i class="fa fa-map-marker"></i> {{$jobs->location}}</span>
						<span><i class="fa fa-money"></i>{{$jobs->price}}</span>
					</div>
					<p>{{$jobs->description}}</p>
				</div>
				


	</div>
	</div>


	<!-- Widgets -->
	<div class="five columns">

		<!-- Sort by -->
		<div class="widget">
			<h4>Overview</h4>

			<div class="job-overview">

				<ul>
					<li>
						<i class="fa fa-map-marker"></i>
						<div>
							<strong>Location:</strong>
							<span>{{$jobs->location}}</span>
						</div>
					</li>
					<li>
						<i class="fa fa-user"></i>
						<div>
							<strong>Job Title:</strong>
							<span>{{$jobs->title}}</span>
						</div>
					</li>
					<li>
						<i class="fa fa-clock-o"></i>
						<div>
							<strong>Hours:</strong>
							<span>40h / week</span>
						</div>
					</li>
					<li>
						<i class="fa fa-money"></i>
						<div>
							<strong>Rate:</strong>
							<span>{{$jobs->price}}ksh</span>
						</div>
					</li>
				</ul>
				@endforeach



				<a href="#small-dialog" id="#small-dialog" class="popup-with-zoom-anim button">Apply For This Job</a>

				<div id="small-dialog" class="zoom-anim-dialog mfp-hide apply-popup">
					<div class="small-dialog-headline">
						<h2>Apply For This Job</h2>
					</div>

					<div class="small-dialog-content">

						{{Form::open(array('route'=>array('apply.store'),'method'=>'post', 'files'=>'true'))}}

						<input type="text" name="amount" id="amount" value="{{old('amount')}}"  placeholder="Amount"/>
{{--
						<input type="text"  name="time" id="time" value="{{old('time')}}" placeholder="Time" />
--}}
						<div class="form-group-sm">
							<label>Time</label>
							<Select name="time" value="{{old('time')}}">
								<option value="Less than 1hr" >  Less than 1hr </option>
								<option value="Less than 3hrs">   Less than 3hrs </option>
								<option value="Less than 24hrs">   Less than 24hrs </option>
								<option value="Less than 3days">   Less than 3days </option>
								<option value="Less than 1 week">   Less than 1 week </option>
								<option value="1 week to 4 weeks">   1 week to 4 weeks </option>
								<option value="1 month to 3 months">  1 month to 3 months </option>
								<option value="3 months to 6 months">  3 months to 6 months </option>
								<option value="More than 6 months">     More than 6 months </option>
							</Select>
						</div>
						<textarea  name="message" value="{{old('message')}}" id="message" placeholder="Your message / cover letter sent to the employer"></textarea>

						<!-- Upload CV -->
						<div class="upload-info"><strong>Upload your CV (optional)</strong> <span>Max. file size: 5MB</span></div>
						<div class="clearfix"></div>

						<label class="upload-btn">
							<input type="file" name="resume" id="resume" value="{{old('resume')}}" multiple />
							<i class="fa fa-upload"></i> Browse
						</label>
						<span class="fake-input">No file selected</span>

						<div class="divider"></div>

						@foreach($job as $jobs)

							<input type="hidden" name="job_id"  value="{{$jobs->id}}" />
							<input type="hidden" name="creator_id" id="creator_id" value="{{$jobs->creator_id}}" />

							@endforeach

						<button type="submit" class="submit send">Send Application</button>
						{{Form::close()}}
					</div>

				</div>

			</div>

		</div>

	</div>
	<!-- Widgets / End -->


</div>
	</div>


@include('includes.footer')